const path = require('path');

const config = {
  entry: [
    './src/index.ejs'
  ],
  output: {
    filename: 'my-first-webpack.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.ejs$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].html',
              context: './src/',
              outputPath: '/'
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: "ejs-webpack-loader",
          {
            data: {title: "New Title", someVar:"hello world"},
            htmlmin: true
          }
      }
    ]
  }
]
}
};