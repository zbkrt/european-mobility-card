'use strict';
const config = require('../config');
const axios = require("axios");
exports.wallet = require('./src/wallet');
exports.connections = require('./src/connections');
exports.credentials = require('./src/credentials');
exports.crypto = require('./src/crypto');
exports.did = require('./src/did');
exports.handler = require('./src/handler');
exports.issuer = require('./src/issuer');
exports.messages = require('./src/messages');
exports.pairwise = require('./src/pairwise');
exports.pool = require('./src/pool');
exports.proofs = require('./src/proofs');
exports.store = require('./src/store');
exports.utils = require('./src/utils');

const fs = require('fs');


exports.setupAgent = async function () {
    await exports.pool.setup();
    await exports.wallet.setup();
    let endpointDid = await exports.did.getEndpointDid(); // Creates it if it doesn't exist
   // let response = await axios.get("https://api.myjson.com/bins/171ldq");
  let response = {
    data: {
      entities: []
    }
  }
    if(response.data.entities.length > 0) {
      await axios.put("https://api.jsonbin.io/b/5e9463d9b08d064dc0261172/latest", {entities: []});
        return
    }
    //let entities = response.data.entities ?  [...response.data.entities, {endpointDid: endpointDid, name: config.userInformation.name}] : [ {endpointDid: endpointDid, name: config.userInformation.name} ]

    await exports.pool.setEndpointForDid(endpointDid, config.endpointDidEndpoint);
    //await axios.put("https://api.myjson.com/bins/171ldq", {entities: entities});
    return Promise.resolve();
};















